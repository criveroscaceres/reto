package com.reto.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInfo extends RetoData{

	@JsonProperty("first_name")
	private String firstName;
	private String avatar;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	
 	
}
