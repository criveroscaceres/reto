package com.reto.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.demo.dto.RetoData;
import com.reto.demo.dto.UserInfo;
import com.reto.demo.response.ResponseReto;
import com.reto.demo.response.ResponseUserApi;
import com.reto.demo.service.RetoService;

@Service
public class RetoServiceImpl implements RetoService {
	
	
	private Environment env;
	
	@Autowired
	public RetoServiceImpl(Environment env) {
		this.env = env;
	}

	@Override
	public ResponseReto reestructurarResponse() {
		
		ResponseReto responseReto = new ResponseReto();
		List<RetoData> data = new ArrayList<>();
		
		ResponseUserApi respUserApi = getResponseUserApi(env);
		
		respUserApi.getData().stream().forEach(item -> {
			
			RetoData retoDataObj = generarRetoData(item);
			data.add(retoDataObj);
			
		});
		
		responseReto.setData(data);
		
		return responseReto;
	}

}
