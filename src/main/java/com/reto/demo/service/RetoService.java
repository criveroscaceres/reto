package com.reto.demo.service;

import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.reto.demo.dto.RetoData;
import com.reto.demo.dto.UserInfo;
import com.reto.demo.response.ResponseReto;
import com.reto.demo.response.ResponseUserApi;

public interface RetoService {
	
	ResponseReto reestructurarResponse();
	
	default ResponseUserApi getResponseUserApi(Environment env) {
		String rutaapiUser = env.getProperty("prueba.apiuser.url");	
		RestTemplate restTemplaet = new RestTemplate();
		ResponseEntity<ResponseUserApi> response = restTemplaet.getForEntity(rutaapiUser, ResponseUserApi.class);
		return response.getBody();
	}
	
	default RetoData generarRetoData(UserInfo userInfo) {
		
		RetoData data = new RetoData();
		data.setEmail(userInfo.getEmail());
		data.setId(userInfo.getId());
		data.setLastName(userInfo.getLastName());
		return data;
	}

}
