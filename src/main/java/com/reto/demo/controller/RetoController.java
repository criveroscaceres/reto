package com.reto.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.demo.response.ResponseReto;
import com.reto.demo.service.RetoService;

@RestController
@RequestMapping("/reto")
public class RetoController {

	
	private RetoService retoService;
	
	@Autowired
	public RetoController(RetoService retoService) {
		this.retoService = retoService;
	}
	
	@PostMapping("/reestructurar")
	ResponseEntity<ResponseReto> reestruturarResponseUserApi(){
		
		ResponseReto response = retoService.reestructurarResponse();
		return ResponseEntity.ok(response);
		
	}
	
}
