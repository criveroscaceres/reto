package com.reto.demo.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.reto.demo.dto.UserInfo;

public class ResponseUserApi {

	private int page;
	
	@JsonProperty("per_page")
	private int perPage;
	
	private int total;
	
	@JsonProperty("total_pages")
	private int totalPages; 
	
	private List<UserInfo> data;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPerPage() {
		return perPage;
	}

	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public List<UserInfo> getData() {
		return data;
	}

	public void setData(List<UserInfo> data) {
		this.data = data;
	}
	
	

}
