package com.reto.demo.response;

import java.util.List;

import com.reto.demo.dto.RetoData;

public class ResponseReto {
	
	private List<RetoData> data;

	public List<RetoData> getData() {
		return data;
	}

	public void setData(List<RetoData> data) {
		this.data = data;
	}

}
