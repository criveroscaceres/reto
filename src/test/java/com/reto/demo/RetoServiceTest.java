package com.reto.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.reto.demo.dto.RetoData;
import com.reto.demo.dto.UserInfo;
import com.reto.demo.response.ResponseReto;
import com.reto.demo.response.ResponseUserApi;
import com.reto.demo.service.impl.RetoServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class RetoServiceTest {
	
	
	@Mock
	private Environment env;
	
	RetoServiceImpl retoServiceImpl;
	
	@Before
    public void init() {
		
		when(this.env.getProperty("prueba.apiuser.url")).thenReturn("https://reqres.in/api/users");
		
		retoServiceImpl = new RetoServiceImpl(env);
	}
	
	
	@Test
	public void testReestructurarResponse () {
		
		ResponseReto response =	retoServiceImpl.reestructurarResponse();
		int id = response.getData().get(0).getId();
		assertEquals(1,id);
		
	}
	
	@Test
	public void testGetResponseUserApi () {
		ResponseUserApi resp = retoServiceImpl.getResponseUserApi(env);
		assertEquals(6, resp.getPerPage());
		assertEquals(12, resp.getTotal());
		
	}
	
	@Test
	public void testGenerarRetoData() {
		
		UserInfo userInfo = new UserInfo();
		userInfo.setAvatar("https://reqres.in/img/faces/1-image.jpg");
		userInfo.setEmail("george.bluth@reqres.in");
		userInfo.setFirstName("George");
		userInfo.setId(1);
		userInfo.setLastName("Bluth");
		
		RetoData  data = retoServiceImpl.generarRetoData(userInfo);
		assertEquals("george.bluth@reqres.in",data.getEmail());
		assertEquals(1,data.getId());
		assertEquals("Bluth",data.getLastName());
		
	}
	
	
	
}
