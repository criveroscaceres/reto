package com.reto.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.reto.demo.dto.RetoData;
import com.reto.demo.response.ResponseReto;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RetoControllerTest {
	
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@LocalServerPort
	private int port;
	
	@Test
	public void testRetoReestructura () {
		
		List<RetoData> data = this.restTemplate.postForObject("http://localhost:" + port + "/reto/reestructurar", null, ResponseReto.class).getData();
		int id = data.get(0).getId();
		assertEquals(1, id);
		
		
		/*
		 * {
            "id": 1,
            "last_name": "Bluth",
            "email": "george.bluth@reqres.in"
        },
		 */
		
	}
	
	@Test
	public void testRetoReestructurasSize () {
		
		List<RetoData> data = this.restTemplate.postForObject("http://localhost:" + port + "/reto/reestructurar", null, ResponseReto.class).getData();
		int size = data.size();
		assertEquals(6, size);
	}
	
	
	
	
	
	
	
	
	

}
